from typing import List, Any


def assert_lists_identical(l1: List[Any], l2: List[Any]):
    for i in l1:
        assert i in l2

    for i in l2:
        assert i in l1

    assert len(l1) == len(l2)


class UnorderedListEq:
    def __init__(self, actual: List[Any]):
        self.actual = actual

    def __eq__(self, other):
        if not isinstance(other, list):
            return False

        expected = other

        for i in self.actual:
            if i not in expected:
                self.item_not_found = i
                self.in_expected_list = expected
                return False

        for i in expected:
            if i not in self.actual:
                self.item_not_found = i
                return False

        if len(self.actual) != len(expected):
            self.expected_len = len(expected)
            return False

        return True

    def __repr__(self):
        return f"unordered({self.actual.__repr__()})"


def unordered(actual: List[Any]) -> UnorderedListEq:
    """
    Compare lists in unordered fashion.

    Use it in ``assert`` statements on the left side of ``==``.
    ::

        actual_list = ...
        assert unordered(actual_list) == [1, 2, 3]

    :param actual:
    :return:
    """
    return UnorderedListEq(actual)
