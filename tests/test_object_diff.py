import logging
from collections.abc import Iterator

from object_diff import (
    dict_diff_iter,
    value_diff,
    Diff,
    ChangeType,
    ABSENT_VALUE,
    diff_iter,
    value_diff_iter,
)
from tests.utils import assert_lists_identical, unordered

logger = logging.getLogger(__name__)


def test_diff_eq():
    assert Diff([], ChangeType.INSERT, ABSENT_VALUE, 11) != None
    assert None != Diff([], ChangeType.INSERT, ABSENT_VALUE, 11)

    diff = Diff([], ChangeType.MODIFY, 12, "123")
    diff_in_dict_form = {
        "path": [],
        "change_type": ChangeType.MODIFY,
        "old_value": 12,
        "new_value": "123",
    }
    assert diff.__dict__ == diff_in_dict_form
    assert diff != diff_in_dict_form
    assert diff_in_dict_form != diff

    assert diff == diff

    assert Diff([], ChangeType.MODIFY, True, 0.0) == Diff([], ChangeType.MODIFY, True, 0.0)
    assert Diff([], ChangeType.INSERT, ABSENT_VALUE, 111) == Diff(
        [], ChangeType.INSERT, "old val ignored when INSERT", 111
    )
    assert Diff([], ChangeType.REMOVE, "aaa", ABSENT_VALUE) == Diff(
        [], ChangeType.REMOVE, "aaa", "new val ignored when REMOVE"
    )

    assert Diff([], ChangeType.INSERT, ABSENT_VALUE, "dd") != Diff(
        ["a", "b"], ChangeType.INSERT, ABSENT_VALUE, "dd"
    )
    assert Diff([], ChangeType.MODIFY, "cc", "dd") != Diff([], ChangeType.INSERT, "cc", "dd")
    assert Diff([], ChangeType.MODIFY, 22, "dd") != Diff([], ChangeType.MODIFY, "cc", "dd")
    assert Diff([], ChangeType.MODIFY, 22, "dd") != Diff([], ChangeType.MODIFY, 22, False)


def test_value_diff_no_diff():
    assert value_diff(None, None) is None
    assert value_diff(ABSENT_VALUE, ABSENT_VALUE) is None
    assert value_diff(1, 1) is None
    assert value_diff(True, True) is None
    assert value_diff("aaa", "aaa") is None
    assert value_diff({}, {}) is None
    assert (
        value_diff(
            {"a": 1, "b": "bb", "c": False, "d": None}, {"a": 1, "b": "bb", "c": False, "d": None},
        )
        is None
    )
    assert value_diff([], []) is None
    assert value_diff([1, 2, 3], [1, 2, 3]) is None
    assert value_diff(set(), set()) is None
    assert value_diff({"a", "b", "c"}, {"a", "b", "c"}) is None
    assert value_diff((), ()) is None
    assert value_diff((1, "b", True), (1, "b", True)) is None
    val = [3, 2, 1]
    assert value_diff(val, val) is None


def test_value_diff():
    assert value_diff(ABSENT_VALUE, "aa") == Diff(
        [], ChangeType.INSERT, "old val ignored when INSERT", "aa"
    )
    assert value_diff(123, ABSENT_VALUE) == Diff(
        [], ChangeType.REMOVE, 123, "new val ignored when REMOVE"
    )
    assert value_diff(None, True) == Diff([], ChangeType.MODIFY, None, True)
    assert value_diff({"a"}, None) == Diff([], ChangeType.MODIFY, {"a"}, None)

    assert value_diff(1, 2, ["a", "b"]) == Diff(["a", "b"], ChangeType.MODIFY, 1, 2)

    assert value_diff({"a": 1, "b": "bb"}, {"a": 2, "b": "bbb"}) == Diff(
        [], ChangeType.MODIFY, {"a": 1, "b": "bb"}, {"a": 2, "b": "bbb"}
    )


def test_value_diff_iter():
    diff_it = value_diff_iter(ABSENT_VALUE, "aa")
    assert next(diff_it) == Diff([], ChangeType.INSERT, "old val ignored when INSERT", "aa")
    assert next(diff_it, ABSENT_VALUE) is ABSENT_VALUE

    diff_it = value_diff_iter(123, ABSENT_VALUE)
    assert next(diff_it) == Diff([], ChangeType.REMOVE, 123, "new val ignored when REMOVE")
    assert next(diff_it, ABSENT_VALUE) is ABSENT_VALUE

    diff_it = value_diff_iter({"a": 1, "b": "bb"}, {"a": 2, "b": "bbb"})
    assert next(diff_it) == Diff([], ChangeType.MODIFY, {"a": 1, "b": "bb"}, {"a": 2, "b": "bbb"})
    assert next(diff_it, ABSENT_VALUE) is ABSENT_VALUE

    assert next(value_diff_iter(1, 1), ABSENT_VALUE) is ABSENT_VALUE


def test_dict_diff_one_level():
    old = {
        "a": 1,
        "b": "abc",
        "c": None,
        "d": True,
        "e": "eee",
        "g": "ggg",
    }
    new = {
        "a": 3,
        "b": "bbbNew",
        "c": "ccc",
        "d": False,
        "f": 18,
        "g": None,
    }

    diff_it = dict_diff_iter(old, new)

    assert isinstance(diff_it, Iterator)
    assert_lists_identical(
        list(diff_it),
        [
            Diff(["a"], ChangeType.MODIFY, 1, 3),
            Diff(["b"], ChangeType.MODIFY, "abc", "bbbNew"),
            Diff(["c"], ChangeType.MODIFY, None, "ccc"),
            Diff(["d"], ChangeType.MODIFY, True, False),
            Diff(["e"], ChangeType.REMOVE, "eee", ABSENT_VALUE),
            Diff(["f"], ChangeType.INSERT, ABSENT_VALUE, 18),
            Diff(["g"], ChangeType.MODIFY, "ggg", None),
        ],
    )


def test_diff_with_different_types():
    assert list(diff_iter(None, ["a", "b"])) == [Diff([], ChangeType.MODIFY, None, ["a", "b"])]
    assert list(diff_iter([1, 2, 3], {"a": 1, "b": 2})) == [
        Diff([], ChangeType.MODIFY, [1, 2, 3], {"a": 1, "b": 2})
    ]
    assert list(diff_iter({"a": 1, "b": 2}, (True, 1, "c"))) == [
        Diff([], ChangeType.MODIFY, {"a": 1, "b": 2}, (True, 1, "c"))
    ]


def test_diff_nested_dicts():
    old = {
        "a": 1,
        "nest_rm": {"a": True, "b": "bb", "nest2": {"c": 12,},},
        "nest_mod": {"aa": "aaa", "bb": 12, "nest2": {"aaa": 123, "bbb": True,},},
        "e": "eee",
    }
    new = {
        "a": 3,
        "nest_ins": {"aa": [1, 2, 3]},
        "f": 18,
        "nest_mod": {"aa": "aMod", "cc": 33, "nest2": {"ccc": "cins", "bbb": False,},},
    }

    diff_it = diff_iter(old, new)

    assert isinstance(diff_it, Iterator)
    # assert len([1,2]) == len([1,2,3])
    assert unordered(list(diff_it)) == [
        Diff(["a"], ChangeType.MODIFY, 1, 3),
        Diff(["nest_ins"], ChangeType.INSERT, ABSENT_VALUE, {"aa": [1, 2, 3]}),
        Diff(
            ["nest_rm"],
            ChangeType.REMOVE,
            {"a": True, "b": "bb", "nest2": {"c": 12,},},
            ABSENT_VALUE,
        ),
        Diff(["e"], ChangeType.REMOVE, "eee", ABSENT_VALUE),
        Diff(["f"], ChangeType.INSERT, ABSENT_VALUE, 18),
        Diff(["nest_mod", "aa"], ChangeType.MODIFY, "aaa", "aMod"),
        Diff(["nest_mod", "bb"], ChangeType.REMOVE, 12, ABSENT_VALUE),
        Diff(["nest_mod", "cc"], ChangeType.INSERT, ABSENT_VALUE, 33),
        Diff(["nest_mod", "nest2", "aaa"], ChangeType.REMOVE, 123, ABSENT_VALUE),
        Diff(["nest_mod", "nest2", "bbb"], ChangeType.MODIFY, True, False),
        Diff(["nest_mod", "nest2", "ccc"], ChangeType.INSERT, ABSENT_VALUE, "cins"),
    ]
